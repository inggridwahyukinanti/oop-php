<?php 

require_once('animal.php'); //require once : dia hanya bisa di require satu kali saja
require_once('Ape.php');
require_once('Frog.php');

$sheep = new Animal("shaun"); //untuk menampung object baru dan di class animal ini menerima 1 parameter nama

echo "Nama Hewan : $sheep->name"; // "shaun"
echo "<br>";
echo "Jumlah Kaki Hewan : $sheep->legs"; // 4
echo "<br>";
echo "Pemangsa : $sheep->cold_blooded"; // "no"
echo"<br> <br>";


// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

$objeks1 = new Ape("Kera Sakti");

echo"Nama Hewan : $objeks1->name <br>";
echo"Jumlah Kaki Hewan : $objeks1->legs <br>";
echo"Pemangsa : $objeks1->cold_blooded <br>";
$objeks1->yell();

 echo"<br> <br>";

$objeks2 = new Frog("Buduk");
echo"Nama Hewan : $objeks2->name <br>";
echo"Jumlah Kaki Hewan : $objeks2->legs <br>";
echo"Pemangsa : $objeks2->cold_blooded <br>";
 $objeks2->jump();
?>
